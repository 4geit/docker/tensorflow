FROM tensorflow/tensorflow
MAINTAINER contact@4ge.it

RUN \
    apt-get update -qqy && \
    apt-get install -qqy python-tk gettext && \
    pip -q install arrow
